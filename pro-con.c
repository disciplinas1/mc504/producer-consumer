#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>

#define LOG false
#define DELAY 2
#define GLASS_SIZE 5
#define SEPARATOR "="
#define H_SYMBOL "H"
#define O_SYMBOL "O"
#define WATER_SYMBOL "H2O"

// Global Variables:
pthread_mutex_t mutex;
pthread_cond_t glass_not_full;
pthread_cond_t glass_has_water;
int h_count;
int o_count;
int molecules_in_glass;
int *consumed_water;

int initial_oxygen;
int consumers;
int water_per_consumer;

typedef struct {
    int value;
} consumer_id;

/* Print string repeated times. */
void print_repeated(const char *value, int times, bool break_line) {
    for (int i = 0; i < times; i++) {
        printf("%s", value);
    }

    if (break_line) {
        printf("\n");
    }
}

/* Print remaining atoms. */
void print_producer() {
    printf("PRODUCER AVAILABLES ATOMS:\n");

    int sep_size = 2 * initial_oxygen;
    print_repeated(SEPARATOR, sep_size, true);
    print_repeated(H_SYMBOL, h_count, true);
    print_repeated(SEPARATOR, sep_size, true);
    print_repeated(O_SYMBOL, o_count, true);
    print_repeated(SEPARATOR, sep_size, true);
}

/* Print glass with corresponding numer of molecules. */
void print_glass() {
    printf("GLASS:\n");

    int water_symbol_size = strlen(WATER_SYMBOL);
    for (int i = GLASS_SIZE; i > 0; i--) {
        printf("|");
        if (i <= molecules_in_glass) {
            printf(WATER_SYMBOL);
        } else {
            print_repeated(" ", water_symbol_size, false);
        }

        printf("|\n");
    }

    printf("+");
    print_repeated("-", water_symbol_size, false);
    printf("+\n");
}

/* Print consumers and their state. */
void print_consumers() {
    printf("CONSUMERS:\n");

    int water_symbol_size = strlen(WATER_SYMBOL);
    for (int i = 0; i < consumers; i++) {
        for (int water = 0; water < consumed_water[i]; water++) {
            printf(WATER_SYMBOL);
            printf(" ");
        }

        for (int empty = consumed_water[i]; empty < water_per_consumer; empty++) {
            print_repeated("_", water_symbol_size, false);
            printf(" ");
        }

        if (consumed_water[i] == water_per_consumer) {
            printf("Yeah! I'm happy now");
        } else {
            printf("I'm thirsty!!!");
        }

        printf("\n");
    }
}

/* Print atoms available for producer and the glass. Unlock mutex. */
void print_system_and_unlock_mutex(pthread_mutex_t *mutex) {
    print_producer();
    printf("\n");
    print_glass();
    printf("\n");
    print_consumers();
    printf("\n");
    sleep(DELAY);
    pthread_mutex_unlock(mutex);
    sleep(DELAY);
}

/**
 * Producer thread. Gets atoms, builds water molecules
 * and puts them into the glass.
 */
void *producer(void *args) {
    o_count = initial_oxygen;
    h_count = 2 * o_count;

    while (h_count >= 2 && o_count >= 1) {
        pthread_mutex_lock(&mutex);

        while (molecules_in_glass == GLASS_SIZE) {
            pthread_cond_wait(&glass_not_full, &mutex);
        }

        h_count -= 2;
        o_count -= 1;
        molecules_in_glass += 1;

        if (molecules_in_glass == 1) {
            pthread_cond_signal(&glass_has_water);
        }

        if (LOG) {
            printf("Product created\n");
        }

        print_system_and_unlock_mutex(&mutex);
    }

    return NULL;
}

/* Consumer thread. Drinks water molecules from the glass. */
void *consumer(void *args) {
    consumer_id *cid = (consumer_id *) args;
    int id = cid->value;

    for (int i = 0; i < water_per_consumer; i++) {
        pthread_mutex_lock(&mutex);

        while (molecules_in_glass == 0) {
            pthread_cond_wait(&glass_has_water, &mutex);
        }

        molecules_in_glass -= 1;
        consumed_water[id] += 1;

        if (molecules_in_glass == GLASS_SIZE - 1) {
            pthread_cond_signal(&glass_not_full);
        }

        if (LOG) {
            printf("Product consumed by %lu\n", (unsigned long) pthread_self());
        }

        print_system_and_unlock_mutex(&mutex);
    }

    return NULL;
}

/* Create producer and consumer threads. */
int main(void) {
    printf("How many people want molecules?\n");
    scanf("%d", &consumers);
    printf("How many molecules each one wants?\n");
    scanf("%d", &water_per_consumer);
    initial_oxygen = consumers * water_per_consumer;

    pthread_mutex_init(&mutex, NULL);
    pthread_cond_init(&glass_not_full, NULL);
    pthread_cond_init(&glass_has_water, NULL);

    consumed_water = malloc(consumers * sizeof(int));
    consumer_id **cid = malloc(consumers * sizeof(consumer_id *));
    pthread_t producer_thread;
    pthread_t consumer_threads[consumers];

    pthread_create(&producer_thread, NULL, producer, NULL);
    for (int i = 0; i < consumers; i++) {
        cid[i] = malloc(sizeof(consumer_id));
        cid[i]->value = i;
        pthread_create(&consumer_threads[i], NULL, consumer, cid[i]);
    }

    pthread_join(producer_thread, NULL);
    for (int i = 0; i < consumers; i++) {
        pthread_join(consumer_threads[i], NULL);
        free(cid[i]);
    }

    free(consumed_water);
    free(cid);
    return EXIT_SUCCESS;
}
