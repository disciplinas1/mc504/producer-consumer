CFLAGS ?=  -pthread -Wall -Werror -Wpedantic -Wunused-result

PROGRAMS = pro-con

all: $(PROGRAMS)

pro-con: pro-con.c
	gcc $(CFLAGS) $^ -o $@

%.o: %.c
	gcc $(CFLAGS) -c $< -o $@

clean:
	rm -f *.o $(PROGRAMS)
